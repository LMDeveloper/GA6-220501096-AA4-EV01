export const Encapsulation = () => {
    return (<>
        <div class="bg-neutral-300 grid grid-cols-1 grid-rows-1 
                    w-[1300px] h-full p-2 place-items-center">
            <div class="grid grid-rows-2 place-items-center">

                <h1 class="text-xl">Encapsulamiento</h1>
            </div>

            <div class="py-2">
                <p class="text-base">El encapsulamiento se refiere a la ocultación de 
                los detalles internos de un objeto y la exposición de una interfaz 
                clara para interactuar con él. Los datos y métodos internos de un 
                objeto están encapsulados dentro de la clase, y el acceso a ellos 
                se controla mediante modificadores de acceso (como público, 
                privado o protegido). Esto ayuda a prevenir cambios no deseados y 
                garantiza la integridad de los datos. 
                </p>
            </div>

            <div class="py-2 flex flex-nowrap items-center ">
                <img class="h-[450px]" src="encapsulamiento.png" alt="herencia en POO" />
                <div class="w-[500px]">
                    <p class="text-base px-2">Por norma general en cualquier videojuego al crear nuestro 
                        personaje no podemos modificar libremente el nivel ni el tipo, 
                        por lo cual siempre vamos a iniciar en nivel '0', 
                        y este se debe incrementar progresivamente a medida que 
                        avancemos en el juego, salvo que el usuario decida usar hacks 
                        para hacer trampa, éste no debe tener acceso al nivel de su personaje, 
                        por lo tanto, es un atributo que debemos encapsular.  
                        La sintaxis de Javascript exige nombrar al atributo con un 
                        guion bajo al inicio.
                    </p>
                </div>
            </div>

        </div>
    </>)
}