export const Objects = () => {

    return (
        <>
            <div class="bg-neutral-300 grid grid-cols-1 grid-rows-1 
                    w-[1300px] h-full p-2 place-items-center">
                <div class="grid grid-rows-2 place-items-center">

                    <h1 class="text-xl">Objetos</h1>

                    <p class="text-base py-6">La programación orientada a objetos
                        nos permite reutilizar la clase Pokémon para crear
                        distintos tipos de pokemones.
                    </p>
                </div>

                <div class="pb-12 grid grid-cols-1 place-items-center">
                    <img class="w-78 h-[70px] mb-4" src="objetos.png" alt="tipos de pokemones" />
                    <img class="w-1/2 py-0" src="pokemon-types.png" alt="Tipos de Pokemons" />

                </div>
            </div>
        </>
    )

}