export const Classes = () => {
    return (
        <>
            <div class="bg-neutral-300 grid grid-cols-1 grid-rows-1 
                    w-[1300px] h-full p-2 place-items-center">
                <div class="grid grid-rows-2 place-items-center">

                    <h1 class="text-xl">Clases / Atributos</h1>
                    <p class="text-base py-6">Las clases representan la naturaleza de los objetos, 
                        los cuales tienen propiedades (características) y métodos 
                        (acciones) que pueden realizar. 
                    </p>
                </div>

                <div class="w-72">
                    <img src="pokemon-games.png" alt="pokemon games" />
                    <p class="text-sm pt-6">Un ejemplo práctico con 
                        los juegos de Pokémon sería: </p>
                </div>

                <div class="flex flex-wrap py-8 w-full h-max place-content-center">
                    <img class="w-[250px]" src="3-pokemons.png" alt="Tipos de Pokemons" />
                    <img src="class-pokemon.png" alt="clase pokemon" />
                </div>

            </div>
        </>
    )
}