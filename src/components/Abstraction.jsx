export const Abstraction = () => {
    return (
        <>
            <div class="bg-neutral-300 grid grid-cols-1 grid-rows-1 
                    w-[1300px] h-full p-2 place-items-center">
                <div class="grid grid-rows-2 place-items-center">

                    <h1 class="text-xl">Abstracción</h1>
                </div>

                <div class="py-2">
                    <p class="text-base">Cuando se establece una clase madre, 
                    no nos interesa ser específicos, sabemos en términos generales 
                    cuales son las características y habilidades que puede tener un Pokémon, 
                    pero no estamos definiendo su identidad de manera particular, 
                    este proceso se conoce en POO como abstracción, en el cual se crean 
                    clases que definen propiedades y comportamientos esenciales de un objeto, 
                    mientras que se ocultan los detalles complejos o específicos
                    </p>
                </div>   
               
                <div class="py-2 flex flex-wrap justify-center ">
                    <img src="abstraccion.png" alt="abstraccion en javascript" />
                </div>

            </div>
        </>
    )
}