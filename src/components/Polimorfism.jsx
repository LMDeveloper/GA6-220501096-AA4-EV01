export const Polimorfism = () => {
    return (
        <>
            <div class="bg-neutral-300 grid grid-cols-1 grid-rows-1 
                    w-[1300px] h-full p-2 place-items-center">
                <div class="grid grid-rows-2 place-items-center">

                    <h1 class="text-xl">Polimorfismo</h1>
                    <p class="text-base">El polimorfismo en POO permite que diferentes
                        clases puedan implementar métodos con el mismo nombre,
                        pero cada clase lo hace de manera diferente según su contexto.
                    </p>
                </div>


                <div class="py-12 flex flex-wrap content-center ">
                    <img class="h-auto" src="polimorfismo.png" alt="polimorfismo en javascript" />
                    <div class="w-60">
                        <p class="text-base px-4 py-6 self-center">En los juegos de Pokémon diferentes Pokémon
                            pueden tener el mismo método "atacar()", pero cada uno lo implementa
                            de manera única. Por ejemplo, Pikachu podría lanzar un ataque eléctrico,
                            mientras que Charizard podría lanzar un ataque de fuego.
                            A pesar de que ambos usan el mismo método "atacar()", se comportan de
                            manera diferente según su especie, lo que es un ejemplo de polimorfismo.
                        </p>
                    </div>

                </div>

            </div>
        </>
    )
}