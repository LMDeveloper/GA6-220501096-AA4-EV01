export const Methods = () => {
    return (
        <>
            <div class="bg-neutral-300 grid grid-cols-1 grid-rows-1 
                    w-[1300px] h-full p-2 place-items-center">
                <div class="grid grid-rows-2 place-items-center">

                    <h1 class="text-xl">Métodos</h1>
                </div>

                <p class="text-base py-6">Los métodos definen el comportamiento de la clase, 
                    basicamente son un conjunto de declaraciones para realizar una acción.
                </p>

                <div class="p-12 grid grid-cols-1 place-items-center">
                    <img src="metodos.png" alt="métodos en javascript" />
                    <img src="pokemon-fight.png" alt="métodos en javascript" />
                    
                    
                </div>

            </div>

        </>
    )
}