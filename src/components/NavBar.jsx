import { NavLink } from "react-router-dom";

export const NavBar = () => {

    const textClassStyle = "text-base text-center w-max h-8 pt-1";
    const buttonClassStyle = "grid gap-0 grid-cols-1 w-full m-1 " +
        "bg-white rounded-xl place-items-center p-1";

    return (
        <>
            <div class="bg-orange-400 grid grid-cols-1 w-72 h-screen p-0 justify-self-center place-items-center">
                <NavLink className="nav-link-button w-48" to="/home">
                    <button class={buttonClassStyle}>
                        <h2 class={textClassStyle}>HOME</h2>
                    </button>
                </NavLink>
                <NavLink className="nav-link-button w-48" to="/intro">
                    <button class={buttonClassStyle}>
                        <h2 class={textClassStyle}>Introducción POO</h2>
                    </button>
                </NavLink>
                <NavLink className="nav-link-button w-48" to="/classes">
                    <button class={buttonClassStyle}>
                        <h2 class={textClassStyle}>Clases / Atributos</h2>
                    </button>
                </NavLink>
                <NavLink className="nav-link-button w-48" to="/objects">
                    <button class={buttonClassStyle}>
                        <h2 class={textClassStyle}>Objetos</h2>
                    </button>
                </NavLink>
                <NavLink className="nav-link-button w-48" to="/methods">
                    <button class={buttonClassStyle}>
                        <h2 class={textClassStyle}>Métodos</h2>
                    </button>
                </NavLink>
                <NavLink className="nav-link-button w-48" to="/inheritance">
                    <button class={buttonClassStyle}>
                        <h2 class={textClassStyle}>Herencia</h2>
                    </button>
                </NavLink>
                <NavLink className="nav-link-button w-48" to="/abstraction">
                    <button class={buttonClassStyle}>
                        <h2 class={textClassStyle}>Abstracción</h2>
                    </button>
                </NavLink>
                <NavLink className="nav-link-button w-48" to="/encapsulation">
                    <button class={buttonClassStyle}>
                        <h2 class={textClassStyle}>Encapsulamiento</h2>
                    </button>
                </NavLink>
                <NavLink className="nav-link-button w-48" to="/polimorfism">
                    <button class={buttonClassStyle}>
                        <h2 class={textClassStyle}>Polimorfismo</h2>
                    </button>
                </NavLink>

            </div>
        </>
    )

}