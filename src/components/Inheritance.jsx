export const Inheritance = () => {

    return (
        <>
            <div class="bg-neutral-300 grid grid-cols-1 grid-rows-1 
                    w-[1300px] h-full p-2 place-items-center">
                <div class="grid grid-rows-2 place-items-center">

                    <h1 class="text-xl">Herencia</h1>
                </div>

                <div class="py-2">
                    <p class="text-base text-center	">En el universo de Pokémon existen distintos tipos,
                        no hace falta crear una clase específica para cada uno,
                        gracias a la POO podemos heredar los atributos de una clase
                        madre para compartir los atributos de esta clase.
                    </p>
                </div>   
                <div class="py-2">
                    <p class="text-base text-center	">Con base al ejemplo anterior, la clase Electrico
                        hereda de la clase Pokemon los atributos 'nombre' y 'nivel',
                        pero define de manera predeterminada el tipo 'electrico'.
                    </p>
                </div>
                

                <div class="py-20 flex flex-wrap justify-center ">
                    <img src="herencia-1.png" alt="herencia en javascript" />
                    <img class="w-24 h-24 place-self-center" src="herencia-2.png" alt="Pokemon Electrico" />

                </div>

            </div>
        </>
    )

}