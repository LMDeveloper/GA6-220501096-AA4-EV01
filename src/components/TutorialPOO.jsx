export const TutorialPOO = () => {
    return (
        <>
            <div
                class="bg-neutral-300 grid grid-cols-1 grid-rows-2 
                w-[1300px] h-full p-2 place-items-center">

                <p class="text-center">
                    Anteriormente un programa de computadora se escribía a modo de instrucciones secuenciales,
                    lo cual hacia muy difícil su mantenimiento a medida que el programa iba escalando y el
                    código se hacía más denso, por esa razón surge el paradigma de programación orientada a
                    objetos (POO), el cual concibe la programación de una manera similar al funcionamiento de las
                    cosas/objetos en el mundo real, en donde dividimos el programa en "objetos" que representan
                    cosas reales (o conceptos abstractos).
                </p>

                <p>La POO se compone de los siguientes conceptos fundamentales: </p>

                <div class="py-8">
                    <ul class="list-disc">
                        <li>Clases / Atributos</li>
                        <li>Objetos</li>
                        <li>Métodos</li>
                        <li>Herencia</li>
                        <li>Abstracción</li>
                        <li>Encapsulamiento</li>
                        <li>Polimorfismo</li>
                    </ul>
                </div>

            </div>
        </>
    )
}