export const Home = () => {


    return ( <>
        <div class="bg-neutral-300 grid grid-cols-1 grid-rows-5 w-[1300px] h-full p-2 place-items-center">
            <img class="h-24 w-24" src="sena-logo.png" />
            <h2>ANALISIS Y DESARROLLO DE SOFTWARE (2758333) </h2>
            <h2 class="text-center">Fundamentos en la implementación de componentes frontend,
                HTML, CSS, JS GA6-220501096-AA4-EV01</h2>
            <h3>PRESENTADO POR: LUIS MIGUEL RODRIGUEZ VARGAS</h3>
            <h3>INSTRUCTOR: ING. FERNANDO FORERO GOMEZ</h3>
        </div>
    </>)

}