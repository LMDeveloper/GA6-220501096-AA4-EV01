import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import { Navigate, Route, Routes } from "react-router-dom"
import { NavBar } from './components/NavBar'
import { Home } from './components/Home'
import { TutorialPOO } from './components/TutorialPOO'
import { Classes } from './components/Classes'
import { Objects } from './components/Objects'
import { Methods } from './components/Methods'
import { Inheritance } from './components/Inheritance'
import { Abstraction } from './components/Abstraction'
import { Encapsulation } from './components/Encapsulation'
import { Polimorfism } from './components/Polimorfism'

function App() {

  return (
    <div class="flex flex-nowrap h-screen place-content-center">
      <NavBar />
      <Routes>
        <Route path='home' element={<Home />}/>
        <Route path='intro' element={<TutorialPOO />}/>
        <Route path='classes' element={<Classes />}/>
        <Route path='objects' element={<Objects />}/>
        <Route path='methods' element={<Methods />}/>
        <Route path='inheritance' element={<Inheritance />}/>
        <Route path='abstraction' element={<Abstraction />}/>
        <Route path='encapsulation' element={<Encapsulation />}/>
        <Route path='polimorfism' element={<Polimorfism />}/>
        <Route path='/' element={<Navigate to="/home" />} />

        {/* <Home /> */}
        {/* <TutorialPOO /> */}
        {/* <Classes /> */}
        {/* <Objects /> */}
        {/* <Methods /> */}
        {/* <Inheritance /> */}
        {/* <Abstraction /> */}
        {/* <Encapsulation /> */}
        {/* <Polimorfism /> */}
      </Routes>
    </div>
  )
}

export default App
